#to run this use the command
#python3 main.py main
#or hit run in an ide
import dicom2nifti
import SimpleITK as sitk
import os

#this function is set up to convert dicom series into compressed nifti files in the same folder
def main():
    # this program is fueled by a list of the dicom series to be converted
    # the folder structure I used had the dicom nested two folders into the root folder (and only two folders in)
    # to generate this list I used      ls -lR | grep /     in the base folder in linux
    # then take that list and turn it into "directories.txt"
    file = open("directories.txt", 'r')
#paths = [file.readlines()]
    #prepaths are raw paths. we use this step to get rid of some special characters
    prepaths = ["/home/storage/microloadingTraining/14 Microloading"+(line.strip('\n').strip(':').strip('.') ) for line in file.readlines()]
    #as I generated my list, there were folders without dicom series
    #folders with dicom series had '1500um' or Failure
    paths= [path for path in prepaths if ("um" in path or "Failure" in path) ]
    print(paths)
    #my paths contained information about the dicom series within them so I kept that information as a new name for the file
    #root folder/700 series/patient 701/1000um   this information was transfered to the new file name
    names = [path.replace('/','_') for path in paths]
#names = [name.replace(' ','_') for name in names] the spaces are probably fine
    for path in paths:
#dicom2nifti.dicom_series_to_nifti(path,path)
#os.system('dcm2niix "{}"'.format(path))
        # here we form a conversion request to a library called dcm2niix that I installed into my linux terminal
        cmd='dcm2niix -z y "{}"'.format(path)
#print(cmd)
        # we serve the command to the linux terminal
        os.system(cmd)
        #fyi ")" does not work in cmd. That is, remove brackets from the name or path otherwise you will have to modify the code
#os.system('dcm2niix -z y "/home/storage/microloadingTraining/14 Microloading/1000 Series/1001_L1,L2,L3_Healthy_ZA_Microloading/0um/"')
    print(names)

#this mainf function tried to use the python dicom2nifti library
#the library threw too many errors in regards to the conversion and was retired.
def mainf():
    file = open("directories.txt", 'r')
    #paths = [file.readlines()]
    prepaths = ["/home/storage/microloadingTraining/14) Microloading"+(line.strip('\n').strip(':').strip('.') ) for line in file.readlines()]
    paths= [path for path in prepaths if ("um" in path or "Failure" in path) ]
    print(paths)
    names = [path.replace('/','_') for path in paths]
    #names = [name.replace(' ','_') for name in names] the spaces are probably fine
    reader = sitk.ImageSeriesReader()
    #dicom_names = reader.GetGDCMSeriesFileNames('M0_1')
    #reader.SetFileNames(dicom_names)
    #image = reader.Execute()
    for path in paths:
        dicom2nifti.dicom_series_to_nifti(path,path,compression=True)
        #sitk.WriteImage(image, 'M0_1.nii.gz')
    print(names)

if __name__ == "__main__":
    main()
